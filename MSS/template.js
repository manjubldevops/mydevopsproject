const { writeFileSync} = require('fs');


let config = `
stages:
    - PAF
`;

for(let filename of tst_files) {

config += `
${filename}-PAF:
  stage: PAF
  script:
    - 'echo "Executing the stage ${filename}"'
    - cd $currentapp
    - ./${filename}.bat
  environment:
        name: $currentenv
  rules:
         - if: '$CI_PIPELINE_SOURCE == "schedule"'
           when: manual
         - if: '$CI_PIPELINE_SOURCE == "pipeline"'
           when: delayed
           start_in: '3 minutes'
         - if: '$CI_COMMIT_BRANCH =~ /^(svt|hfsvt|master|test|stage)$/'         
  tags:
        - $RUNNER_PAF_WIN
  artifacts:
        paths:
            - ./$currentapp/report/

`;

}
writeFileSync('./dynamic-gitlab-ci.yml', config, {encoding: 'utf-8'});
